from django.shortcuts import render
from .models import Producto
# Create your views here.
def galeria(request):
    productos = Producto.objects.all()
    data = {
        'productos': productos
    }
    return render(request, 'app/galeria.html', data)

def home(request):
    return render(request, 'app/home.html')

def contacto(request):
    return render(request, 'app/contacto.html')

def gal(request):
    return render(request, 'app/gal.html')

